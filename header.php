<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 */
?><!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>
		<?php wp_title('|', true, 'right'); ?>
	</title>
	<meta name="viewport" content="width=device-width">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
	<!--wordpress head-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="main-wrapper" class="main-wrapper">
		<header id="masthead" class="site-header" role="banner">
			<nav class="navbar  navbar-default" role="navigation">
				<div class="container">
					<div class="row">
						<div class="navbar-header col-md-2">
							<div class="site-title-heading">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<?php if ( function_exists( 'ot_get_option' ) && ot_get_option( 'logo' ) ): ?>
										<img src="<?php echo ot_get_option( 'logo' ); ?>" />
									<?php else: ?>
											<?php bloginfo('name'); ?>
									<?PHP endif; ?>
								</a>
							</div>

							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-primary-collapse">
								<span class="sr-only"><?php _e('Toggle navigation', 'wp_base'); ?></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
										
						<div class="col-md-10">
							<div class="collapse navbar-collapse navbar-primary-collapse">
								<?php 
								wp_nav_menu(array(
									'theme_location' => 'primary', 
									'container' => false, 
									'menu_class' => 'nav navbar-nav', 
									'walker' => new MyWalkerNavMenu())
								); 
								?> 
							</div><!--.navbar-collapse-->
						</div>
					</div>
				</div>
			</nav>
		</header><!-- #masthead -->

		<div id="main" class="site-main">
