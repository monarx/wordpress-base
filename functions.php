<?php
/**
 * WPBase theme
 * 
 * @package WPBase
 */

$appPath = pathinfo(__FILE__);
$site_url = get_site_url();
define("WPBASE_APP_PATH", $appPath['dirname']);
define("WPBASE_SYS_DIR", WPBASE_APP_PATH . "/includes");
define("BOWER", WPBASE_APP_PATH . "/assets/bower_components");

require_once WPBASE_SYS_DIR . "/MyWalkerNavMenu.php";
# require_once WPBASE_SYS_DIR . "/widgets.php";

/**
 * Define the options-tree
 */
define( "OPTIONS", false );

if (!isset($content_width)) {
	$content_width = 1024;
}



if (!function_exists('themeSetup')) {
	/**
	 * Setup theme and register support wp features.
	 */
	function themeSetup() 
	{
		/**
		 * Make theme available for translation
		 * Translations can be filed in the /languages/ directory
		 * 
		 * copy from underscores theme
		 */
		load_theme_textdomain('wp-base', get_template_directory() . '/languages');

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'image', 'video', 'quote', 'link', 'gallery', 'audio'
		) );

		// add theme support post and comment automatic feed links
		add_theme_support('automatic-feed-links');

		// enable support for post thumbnail or feature image on posts and pages
		add_theme_support('post-thumbnails');

		if ( function_exists( 'add_image_size' ) ) {
		}

		// add support menu
		register_nav_menus(array(
			'primary' => __('Primary Menu', 'wp_base'),
		));
	}
}
add_action('after_setup_theme', 'themeSetup');



/**
 * Option Tree Plugin
 *
 * - ot_show_pages:      will hide the settings & documentation pages.
 * - ot_show_new_layout: will hide the "New Layout" section on the Theme Options page.
 */

if ( ! OPTIONS ) {
	add_filter( 'ot_show_pages', '__return_false' );
	add_filter( 'ot_show_new_layout', '__return_false' );
}

// Required: set 'ot_theme_mode' filter to true.
add_filter( 'ot_theme_mode', '__return_true' );

// Required: include OptionTree.
load_template( BOWER . '/option-tree/ot-loader.php' );



// Load the options file
if ( ! OPTIONS ) {
	load_template( trailingslashit( get_template_directory() ) . 'includes/theme-options.php' );
}

if (!function_exists('GetMainColumnSize')) {
	/**
	 * Determine main column size from actived sidebar
	 * 
	 * For theme designer:
	 * By using this column size, Bootstrap grid size is 12. 
	 * You may change grid size of sidebar column to number you want; example sidebar-left.php grid 3, sidebar-right.php grid 3.
	 * 
	 * @return integer return column size.
	 */
	function GetMainColumnSize() 
	{
		if (is_active_sidebar('sidebar-left') && is_active_sidebar('sidebar-right')) {
			// if both sidebar actived.
			$main_column_size = 6;
		} elseif (
				(is_active_sidebar('sidebar-left') && !is_active_sidebar('sidebar-right')) || 
				(is_active_sidebar('sidebar-right') && !is_active_sidebar('sidebar-left'))
		) {
			// if only one sidebar actived.
			$main_column_size = 9;
		} else {
			// if no sidebar actived.
			$main_column_size = 12;
		}

		return $main_column_size;
	}
}

if (!function_exists('WPBaseEnqueueScripts')) {
	/**
	 * Enqueue scripts & styles
	 */
	function WPBaseEnqueueScripts() 
	{
		global $wp_scripts;

		wp_enqueue_style(
			'font-awesome', 
			'http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
			array(),
			'4.3.0'
		);
		wp_enqueue_style(
			'main-styles', 
			get_template_directory_uri() . '/assets/css/main.css'
		);

		wp_enqueue_script('jquery');
		wp_enqueue_script(
			'tw-bootstrap', 
			get_template_directory_uri() . BOWER . '/bootstrap-sass/assets/javascripts/bootstrap.min.js',
			array('jquery'),
			'3.3.5'
		);

		wp_enqueue_script(
			'modernizr', 
			get_stylesheet_directory_uri() . BOWER . '/modernizr/modernizr.js',
			array('jquery'),
			'2.7.1'
		);

		// Register the script
		wp_register_script( 
			'html5shiv', 
			get_stylesheet_directory_uri() . BOWER . '/html5shiv/dist/html5shiv.min.js', 
			array(), 
			NULL 
		);
		// Attempt to add in the IE conditional tags
		$wp_scripts->add_data('html5shiv', 'conditional', 'lt IE 9');
		// Enqueue the script
		wp_enqueue_script( 'html5shiv' );

		wp_enqueue_script(
			'main-script', 
			get_template_directory_uri() . '/assets/js/main.js',
			array('jquery', 'tw-bootstrap'),
			'3.3.5'
		);
	}
}
add_action('wp_enqueue_scripts', 'WPBaseEnqueueScripts');

if (!function_exists('WidgetsInit')) {
	/**
	 * Register widget areas
	 */
	function WidgetsInit() 
	{
		register_sidebar(array(
			'name'          => __('Header widget area', 'wp_base'),
			'id'            => 'header',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<div class="widget-title">',
			'after_title'   => '</div>',
		));

		register_sidebar(array(
			'name'          => __('Sidebar left', 'wp_base'),
			'id'            => 'sidebar-left',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<div class="widget-title">',
			'after_title'   => '</div><div class="widget-content">',
		));

		register_sidebar(array(
			'name'          => __('Sidebar right', 'wp_base'),
			'id'            => 'sidebar-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<div class="widget-title">',
			'after_title'   => '</div><div class="widget-content">',
		));

		register_sidebar(array(
			'name'          => __('Footer widget area', 'wp_base'),
			'id'            => 'footer',
			'before_title'  => '<h2 class="section-title text-center">',
			'after_title'   => '</h2>',
		));
	}
}
add_action('widgets_init', 'WidgetsInit');

function wpbase_class_names($classes) {
	if ( wp_is_mobile() ) {
		$classes[] = 'mobile';
	} else {
		$classes[] = 'not-mobile';
	}

	if ( is_front_page() ) {
		$classes[] = 'front-page';
	} else {
		$classes[] = 'not-front-page';
	}

	return $classes;
}
add_filter('body_class','wpbase_class_names');

if (!function_exists('EditPostLink')) {
	/**
	 * Display edit post link
	 * 
	 * @return string
	 */
	function EditPostLink() 
	{
		return edit_post_link(
			'<i class="edit-post-icon fa fa-pencil" title="' . __('Edit', 'wp_base') . '"></i>', 
			'<span class="edit-post-link btn btn-app" title="' . __('Edit', 'wp_base') . '">', 
			'</span>'
		);
	}
}

function wp_base_content_nav() {
	global $wp_query, $wp_rewrite;

	$paged			=	( get_query_var( 'paged' ) ) ? intval( get_query_var( 'paged' ) ) : 1;

	$pagenum_link	=	html_entity_decode( get_pagenum_link() );
	$query_args		=	array();
	$url_parts		=	explode( '?', $pagenum_link );
	
	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}
	$pagenum_link	=	remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link	=	trailingslashit( $pagenum_link ) . '%_%';
	
	$format			=	( $wp_rewrite->using_index_permalinks() AND ! strpos( $pagenum_link, 'index.php' ) ) ? 'index.php/' : '';
	$format			.=	$wp_rewrite->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';
	
	$links	=	paginate_links( array(
		'base'		=>	$pagenum_link,
		'format'	=>	$format,
		'total'		=>	$wp_query->max_num_pages,
		'current'	=>	$paged,
		'mid_size'	=>	3,
		'type'		=>	'array',
	) );

	if( is_array( $links ) ) {
		echo '<nav class="pagination pagination-centered clearfix"><ul class="pagination">';
		foreach ( $links as $link ) {
			$pos = strpos($link, 'current');
			if ($pos === false) {
				echo "<li>$link</li>";
			} else {
				echo "<li class=\"active\">$link</li>";
			}
		}
		echo '</ul></nav>';
	}
}

if ( ! function_exists( 'posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author,
* comment and edit link
*
* @author	Konstantin Obenland
* @since	1.0.0 - 05.02.2012
*
* @return	void
*/
function posted_on() {
	printf( __( '<span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'the-bootstrap' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'the-bootstrap' ), get_the_author() ) ),
			get_the_author()
	);
	if ( comments_open() AND ! post_password_required() ) { ?>
		<span class="sep"> | </span>
		<span class="comments-link">
			<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a reply', 'the-bootstrap' ) . '</span>', __( '<strong>1</strong> Reply', 'the-bootstrap' ), __( '<strong>%</strong> Replies', 'the-bootstrap' ) ); ?>
		</span>
		<?php
	}
	edit_post_link( '<i class="fa fa-pencil"></i>' . __( 'Edit', 'the-bootstrap' ), '<span class="sep">&nbsp;</span><span class="edit-link label">', '</span>' );
}
endif;
