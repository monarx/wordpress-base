jQuery(function($){
	// Bootstrap plugins
	$( '#content [rel="tooltip"]' ).tooltip();
	$( '#content [rel="popover"]' ).popover();
	$( '.alert' ).alert();
	$( '.carousel-inner figure:first-child' ).addClass( 'active' );
	$( '.carousel' ).carousel();
});