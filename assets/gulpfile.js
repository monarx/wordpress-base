// Basic Gulp File
//
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    minifyCss = require('gulp-minify-css'),
    notify = require("gulp-notify"),
    concat = require("gulp-concat"),
    uglify = require('gulp-uglify');

var bootstrapDir = './bower_components/bootstrap-sass/assets/javascripts/bootstrap';
var config = {
    sassPath: './css/scss',
    bowerDir: './bower_components',
    scripts: [
        /* 
            Bootstrap scripts
        */        
        bootstrapDir + '/transition.js',
        bootstrapDir + '/affix.js',
        bootstrapDir + '/alert.js',
        //bootstrapDir + '/button.js',
        //bootstrapDir + '/carousel.js',
        bootstrapDir + '/collapse.js',
        bootstrapDir + '/dropdown.js',
        bootstrapDir + '/modal.js',
        //bootstrapDir + '/scrollspy.js',
        bootstrapDir + '/tab.js',
        bootstrapDir + '/tooltip.js',        
        bootstrapDir + '/popover.js',

    	/*
			Site scripts
    	*/
    	'./bower_components/modernizr/modernizr.js',
    	'./js/main.js',
    ],
    styles: [
        './css/scss/main.scss',
    ],
}

gulp.task('scripts', function() {
	return gulp.src(config.scripts)
				.pipe(concat('main.min.js'))
				//.pipe(uglify())
				.pipe(gulp.dest('./js/'));
});

gulp.task('css', function() {
    return gulp.src(config.styles)
        .pipe(sourcemaps.init())
        .pipe(sass({
            style: 'compressed',
            loadPath: [
                './css/scss',
            ]
        })
        .on("error", notify.onError(function (error) {
            return "Error: " + error.message;
        })))        
        .pipe(minifyCss())        
        .pipe(gulp.dest('./css/maps'))
        .pipe(sourcemaps.write('./maps'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(config.sassPath + '/**/*.scss', ['css']);
    gulp.watch(config.scripts, ['scripts']);
});

gulp.task('default', ['css', 'scripts']);
