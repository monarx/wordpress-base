		</div>
	</div>
	<footer id="site-footer" class="footer" role="contentinfo">
		<div class="copy">
			<div class="container">
				<span>
					<?php echo '&copy; ' . date('Y') . ' ' . esc_attr(get_bloginfo('name', 'display')) . ', ' . __('all right recerved', 'wp_base'); ?>
				</span>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>
</body>
</html>