<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 *
 * @package WordPress
 * @subpackage WP_base
 * @since WP Base 1.0
 */
$main_column_size = GetMainColumnSize();

get_header(); ?>

<div class="container">
	<div class="row">
		<?php get_sidebar('left'); ?>

		<div class="col-md-<?php echo $main_column_size; ?> content-area" id="main-column">
			<?php if ( have_posts() ) :

				/* The loop */
				while ( have_posts() ) : the_post();
					get_template_part( '/partials/content', get_post_format() );
				endwhile; 

				wp_base_content_nav('nav-below' );

			else: 
				get_template_part( 'content', 'none' ); 
			endif; ?>
		</div>

		<?php get_sidebar('right'); ?>
	</div>
</div>

<?php get_footer(); ?>